LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := nativetest-ndk-build
NDK_LIBS_OUT=src/main/jniLibs
#LOCAL_SRC_FILES := my-native-lib.cpp
LOCAL_SRC_FILES := $(subst $(LOCAL_PATH)/,,$(wildcard $(LOCAL_PATH)/*.cpp))


include $(BUILD_SHARED_LIBRARY)