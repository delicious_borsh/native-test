#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_ponykamni_nativetest_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from the other side";
    return env->NewStringUTF(hello.c_str());
}