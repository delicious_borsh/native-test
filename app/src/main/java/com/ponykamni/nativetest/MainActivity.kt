package com.ponykamni.nativetest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val text = stringFromJNI()

        findViewById<TextView>(R.id.text).text = text
    }

    private external fun stringFromJNI(): String

    companion object {

        init {
            System.loadLibrary("nativetest-ndk-build")
//            System.loadLibrary("nativetest-cmake")
        }
    }
}